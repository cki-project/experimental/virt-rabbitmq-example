"""Virt rabbitmq example main entrypoint."""
import json
import logging
import os
import platform
import random
import ssl
import uuid

import pika


def callback(body=None, **_):
    """Print message bodies."""
    print(body)


def main():
    # pylint: disable=too-many-locals
    """Run the main function for the example."""
    host = os.environ['RABBITMQ_HOST'].split()
    port = int(os.environ['RABBITMQ_PORT'])
    virtual_host = os.environ['RABBITMQ_VIRTUAL_HOST']
    user = os.environ['RABBITMQ_USER']
    password = os.environ['RABBITMQ_PASSWORD']
    cafile = os.environ['RABBITMQ_CAFILE']

    exchange = os.environ['RABBITMQ_EXCHANGE']
    routing_keys = os.environ['RABBITMQ_ROUTING_KEYS'].split()

    connection_params = [pika.ConnectionParameters(
        host=h.rstrip('/'), port=port, virtual_host=virtual_host,
        credentials=pika.PlainCredentials(user, password),
        client_properties={'connection_name': platform.node()},
        ssl_options=pika.SSLOptions(ssl.create_default_context(cafile=cafile))
    ) for h in host]
    random.shuffle(connection_params)

    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()

    production = True
    if production:
        # production queue, as durable as possible to not lose msgs
        queue_name = 'virt.queue.example'
        channel.queue_declare(queue_name, durable=True)
    # or: temporary queue, uuid format
    else:
        queue_name = str(uuid.uuid4())
        channel.queue_declare(queue_name, auto_delete=True)

    for routing_key in routing_keys:
        channel.queue_bind(queue_name, exchange, routing_key=routing_key)

    for method, properties, body in channel.consume(queue_name):
        try:
            callback(body=json.loads(body),
                     routing_key=method.routing_key,
                     headers=properties.headers)
            channel.basic_ack(method.delivery_tag)
        # pylint: disable=broad-except
        except Exception:
            logging.exception('Message handling failure, will be retried after restart')


if __name__ == '__main__':
    main()
